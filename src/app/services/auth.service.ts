import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Variables
  CLIENT_URL: string = environment.apiUrl + environment.apiClientToken;
  LOGOUT_URL: string = environment.apiUrl + environment.authLogout;
  options: any;

   /**
   * Constructor
   * @param http The http client object
   */
  constructor(private http: HttpClient){
    this.options={
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      })
    };
  }

    /**
   * Get an access token
   * @param e The email address
   * @param p The password string
   */
  login(e: string, p:string){
    return this.http.post(this.CLIENT_URL, {
      grant_type:'password',
      client_id: environment.apiClientId,
      client_secret: environment.apiSecret,
      username: e,
      password: p,
      remember_me: false,
      scope: ''
    }, this.options);
  }

  /**
   * Revoke the authenticated user token
   */
  logout(){
    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json2',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get(this.LOGOUT_URL, options);
  }

}
