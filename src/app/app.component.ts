import { Component } from '@angular/core';

import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Shacks Homepage';
  authUser: boolean;
  loading: boolean;

  constructor(private authService: AuthService,
    private router: Router){


    if (localStorage.getItem('access_token'))
    {
      this.authUser=true;
    } else{
      this.authUser=false;
    }
  }

  logout(): void {
    this.loading = true;
    this.authService.logout()
      .subscribe(() => {
        this.loading = false;
        localStorage.removeItem('access_token');
        this.router.navigate(['/']);
        this.authUser=false;
      });
  }

}
