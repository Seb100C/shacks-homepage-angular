import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestGuardService } from './services/guest-guard.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    children:[
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'blog',
        loadChildren: () => import('./blog/blog.module').then(m=> m.BlogModule)
      },
      {
        path: 'login', component: LoginComponent,
        //canActivate: [ GuestGuardService ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
