import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogComponent } from './blog/blog.component';
import { NewpostComponent } from './newpost/newpost.component';
import { PostComponent } from './post/post.component';
import { Page404blogComponent } from './page404blog/page404blog.component';

const routes: Routes = [

  {
    path: ''
    , component: BlogComponent
  },
  {
    path: 'newpost', component: NewpostComponent
  },
  {
    path: 'post', component: PostComponent
  },
  {
    path: '**', component: Page404blogComponent
  }


  /*{
    path: ''
    , component: BlogComponent,
    children:
    [
      {
        path: 'newpost', component: NewpostComponent
      },
      {
        path: 'post', component: PostComponent
      },
      {
        path: '**', component: Page404blogComponent
      }
    ] 
  }*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
