import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module'
import { BlogComponent } from './blog/blog.component';
import { PostComponent } from './post/post.component';
import { NewpostComponent } from './newpost/newpost.component';
import { PostCardComponent } from './post-card/post-card.component';
import { Page404blogComponent } from './page404blog/page404blog.component';



@NgModule({
  declarations: [
    BlogComponent,
    PostComponent,
    NewpostComponent,
    PostCardComponent,
    Page404blogComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule
  ]
})
export class BlogModule { }
