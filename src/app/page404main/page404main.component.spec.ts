import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page404mainComponent } from './page404main.component';

describe('Page404mainComponent', () => {
  let component: Page404mainComponent;
  let fixture: ComponentFixture<Page404mainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page404mainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page404mainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
