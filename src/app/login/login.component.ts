import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loading: boolean;
  errors: boolean;
  message: String;

  loginForm= this.fb.group({
    email: ['a', Validators.email, Validators.required],
    password: ['b', Validators.required]
  });


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router)
    {}

    ngOnInit(): void {

    }

  login(): void {
    this.loading = true;
    this.errors = false;

    this.authService.login(this.loginForm.controls.email.value ,this.loginForm.controls.password.value)
    .subscribe((res:any)=>{
      // Store token
      localStorage.setItem('access_token', res.access_token);
      this.loading=false;
      // to home
      this.router.navigate(['/']);
    }, (err: any) => {
      this.message ='Error auth';
      this.loading=false;
      this.errors=true;
    }
    )


  }
}
