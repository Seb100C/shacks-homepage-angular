// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "http://localhost/shacks-homepage-laravel/public",
  authLogin: "/api/login",
  authSignup: "/api/signup",
  authLogout: "/api/logout",
  apiClientToken: "/oauth/token",
  apiClientId: "9159344c-3c73-4671-bde2-3ac20c75d913",
  apiSecret: "5pqzB1udqQOnUYK5LkpIcmMdyPLejvZGPNGyDrtX"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
